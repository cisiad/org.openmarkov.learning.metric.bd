package org.openmarkov.learning.metric.bd;


import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.action.AddLinkEdit;
import org.openmarkov.core.action.InvertLinkEdit;
import org.openmarkov.core.action.RemoveLinkEdit;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.io.database.CaseDatabase;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.io.database.elvira.ElviraDataBaseIO;


public class BDMetricTest {
	
	ProbNet probNet;
	
	BDMetric metric;
	
	private double alpha = 0.5;
	
	private String dbFilename = "/learnTestDataBase.dbc";
	
	private final double maxError = 1E-6;
	
	@Before
	public void setUp() throws Exception {
        ElviraDataBaseIO databaseIO = new ElviraDataBaseIO ();
        CaseDatabase database = databaseIO.load(getClass().getResource (dbFilename).getFile ());
        probNet = new ProbNet ();
        
        for(Variable variable: database.getVariables ())
        {
            probNet.addNode (variable, NodeType.CHANCE);
        }        
        ArrayList<Integer> variableIndex = new ArrayList<Integer>();
        for (int i = 0; i < probNet.getNumNodes(); i++){
            variableIndex.add(new Integer(i));
        }
        
        metric = new BDMetric(alpha);
        metric.init (probNet, database);
	}

	@Test
	public void testScores() {
		double score = metric.getScore();
        Assert.assertEquals(score, -4003.30495017, maxError);
	}

	@Test
	public void testScoreLinkAdded() throws NodeNotFoundException {
		AddLinkEdit edition = new AddLinkEdit(probNet, 
				probNet.getVariable("E"), probNet.getVariable("D"), true);
		double score = metric.getScore(edition);
		Assert.assertEquals(score, 257.07201665, maxError);
	}

	@Test
	public void testScoreLinkInverted() throws Exception {
		AddLinkEdit auxiliarEdition = new AddLinkEdit(probNet,
				probNet.getVariable("E"), probNet.getVariable("D"), true);

		probNet.doEdit(auxiliarEdition);
        
		InvertLinkEdit edition = new InvertLinkEdit(probNet, 
				probNet.getVariable("E"), probNet.getVariable("D"), true);
		double score = metric.getScore(edition);
		Assert.assertEquals(score, 4.5474735e-13, maxError);
	}
	
	@Test
	public void testScoreLinkRemoved() throws Exception {
		AddLinkEdit auxiliarEdition = new AddLinkEdit(probNet, 
				probNet.getVariable("D"), probNet.getVariable("E"), true);
		
		probNet.doEdit(auxiliarEdition);
        
		RemoveLinkEdit edition = new RemoveLinkEdit(probNet, 
				probNet.getVariable("D"), probNet.getVariable("E"), true);
		double score = metric.getScore(edition);
		Assert.assertEquals(score, -257.07201665, maxError);
	}
}
